package me.yamakaja.guicedemo;

import com.google.inject.*;

/**
 * Created by Yamakaja on 9/30/17.
 */
@Singleton
public class Bootstrap {

    @Inject
    private DemoEventHandler eventHandler;

    private Injector injector;

    public static void main(String[] args) {
        new Bootstrap().objMain(args);
    }

    public void objMain(String[] args) {
        AbstractModule uiModule = new AbstractModule() {

            @Override
            protected void configure() {
                bind(Bootstrap.class).toInstance(Bootstrap.this);
                bind(UI.class).to(UIImpl.class);
            }

        };

        injector = Guice.createInjector(uiModule);
        injector.injectMembers(this);

        eventHandler.invoke();
    }

    public void doThing() {
        System.out.println("Hello world!");
    }

}
