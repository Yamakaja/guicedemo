package me.yamakaja.guicedemo;

import com.google.inject.Inject;

/**
 * Created by Yamakaja on 9/30/17.
 */
public class DemoEventHandler {

    @Inject
    private Bootstrap demoApplication;

    public void invoke() {
        demoApplication.doThing();
    }

}
