package me.yamakaja.guicedemo;

/**
 * Created by Yamakaja on 9/30/17.
 */
public class UIImpl implements UI {

    @Override
    public void show(String message) {
        System.out.println(message);
    }

}
