package me.yamakaja.guicedemo;

/**
 * Created by Yamakaja on 9/30/17.
 */
public interface UI {

    void show(String message);

}
